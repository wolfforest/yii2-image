Cache images
============
Resize and cache images

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist forest/yii2-image "*"
```

or add

```
"forest/yii2-image": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= Html::img(Yii::$app->img->thumbnail('@webroot/img/img3.jpg', 'wide')) ?>
```

```php
<?= Html::img(Yii::$app->img->thumbnail('@webroot/img/img3.jpg', [
    'width' => 100,
    'height' => 100,
    'quality' => 20,
])) ?>
```