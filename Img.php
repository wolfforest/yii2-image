<?php

namespace forest\image;
use Yii;
use yii\base\Component;
use yii\imagine\Image;

/**
 * This is just an example.
 * @global Yii::$app->img
 */
class Img extends Component
{
    public $tmp_path = '@webroot/assets/';
    public $url_path = '@web/assets/';
    public $file_prefix = 'img';
    public $profile = [
        'mini' => [
            'width' => 90,
            'height' => 90,
            'quality' => 1,
        ],
        'tile' => [
            'width' => 120,
            'height' => 120,
            'file_prefix' => 'tile',
        ],
        'bast' => [
            'width' => 150,
            'height' => 150,
            'quality' => 100,
        ],
        'wide' => [
            'width' => 150,
            'height' => 50,
            'quality' => 90,
        ],
        'product' => [
            'width' => 320,
            'height' => 320,
            'quality' => 90,
        ]
    ];

    public function thumbnail($src, $options = [])
    {
        if(is_string($options) && isset($this->profile[$options])){
            $file_prefix = $options;
            $options = $this->profile[$options];
        }
        $options = (array) $options;
        
        $width = $options['width'] ?: 120;
        $height = $options['height'] ?: 120;
        $quality = $options['quality'] ?: 85;
        //$file_prefix = $options['file_prefix'] ?: $this->file_prefix;
        $tmp_path = $options['tmp_path'] ?: $this->tmp_path;
        $url_path = $options['url_path'] ?: $this->url_path;

        $name = md5($src);
        $file_name = $file_prefix.$width.'x'.$height.'q'.$quality.'_'.$name.'.jpg';
        $save_to = Yii::getAlias($tmp_path.$file_name);
        $url = Yii::getAlias($url_path.$file_name);

        if(!file_exists($save_to)){
            if(!file_exists(Yii::getAlias($src))){
                $src = '@webroot/img/no-img.png';
            }
            $Imagine = Image::getImagine();
            $image = $Imagine->open(Yii::getAlias($src));
            Image::thumbnail($image, $options['width'], $options['height'])
                ->save($save_to, ['quality' => $quality]);
        }

        return $url;
    }
}
